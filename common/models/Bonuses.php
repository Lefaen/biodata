<?php


namespace common\models;

/**
 * Bonuses model
 *
 * @property integer $id
 * @property string $title
 * @property integer $quantity
 */
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Bonuses extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%bonuses}}';
    }

    public static function getRandomBonus()
    {
        $bonuses = self::find()->all();
        $lastIndex = count($bonuses) - 1;
        if($lastIndex > 0){
            do{
                $index = rand(0, $lastIndex);

            }while($bonuses[$index]->quantity === 0);

            if($bonuses[$index]->quantity > 0){
                $bonuses[$index]->quantity = $bonuses[$index]->quantity - 1;
                $bonuses[$index]->save();
            }

            return $bonuses[$index];

        }
    }
}