<?php


namespace common\models;


use frontend\models\interfaces\iSocial;
use frontend\models\interfaces\iSocialProfile;
use frontend\models\Socials\Facebook;
use frontend\models\Socials\Social;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * BiodataUsers model
 *
 * @property integer $id
 * @property integer $bonus_id
 * @property Bonuses $bonus
 * @property string $facebook_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class BiodataUsers extends ActiveRecord
{
    private iSocial $currentSocial;

    private static $isAuth = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%users_biodata}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function login($socialName): bool
    {
        if($this->currentSocial = new Social($socialName)){
            return $this->currentSocial->login();
        }
        return false;
    }

    public function logout($socialName = Facebook::NAME)
    {
        if ($this->currentSocial = new Social($socialName)) {
            $this->currentSocial->logout();
        }
        return false;
    }

    public static function loggedOn(): bool
    {
        if (Yii::$app->session->get(Social::$paramLoggedOn)) {
            return true;
        }
        return false;
    }

    public function register($socialName)
    {
        if (!$this->currentSocial) {
            $this->currentSocial = new Social($socialName);
        }

        return $this->register($socialName);
    }

    public static function isAuth()
    {
        return self::$isAuth ;
    }

    public static function getCurrentUser()
    {
        return Yii::$app->session->get(Social::$paramLoggedOn);
    }

    public function getProfile($socialName) :iSocialProfile
    {
        if ($this->currentSocial = new Social($socialName)) {
            $user = $this->currentSocial->getProfile();
            return $user;
        }
    }

    public static function getUserByFacebookId($id)
    {
        $user = self::findOne(['facebook_id' => $id]);
        return $user;
    }

    public function getBonus()
    {
        return $this->hasOne(Bonuses::class, ['id' => 'bonus_id']);
    }

    public function setBonus()
    {
        $bonus = Bonuses::getRandomBonus()->id;
        $this->bonus_id = $bonus;
        return $bonus;
    }
}