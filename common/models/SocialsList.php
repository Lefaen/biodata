<?php


namespace common\models;


use frontend\models\Socials\Facebook;

class SocialsList
{
    private static $socials = [
        Facebook::NAME => Facebook::class,
    ];

    public static function list()
    {
        return static::$socials;
    }
}