<?php

/* @var $this yii\web\View */
/* @var $users \common\models\BiodataUsers */

$this->title = 'Users';
?>
<table>
    <tr>
        <th>Имя</th>
        <th>Дата регистрации</th>
        <th>Бонус</th>
    </tr>
    <?php foreach ($users as $user):?>
    <tr>
        <td><?=$user->facebook_id;?></td>
        <td><?=Yii::$app->formatter->asDatetime($user->created_at);?></td>
        <td><?=($user->bonus->title??'none');?></td>
    </tr>
    <?php endforeach;?>
</table>
