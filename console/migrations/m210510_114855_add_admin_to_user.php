<?php

use yii\db\Migration;

/**
 * Class m210510_114855_add_admin_to_user
 */
class m210510_114855_add_admin_to_user extends Migration
{
    public function up()
    {
        $user = new \common\models\User();
        $user->username = 'admin';
        $user->email = 'lefaen@gmail.com';
        $user->setPassword('secret');
        $user->generateAuthKey();
        $user->status = \common\models\User::STATUS_ACTIVE;
        $user->save();
    }

    public function down()
    {
        echo "m210510_114855_add_admin_to_user cannot be reverted.\n";

        return false;
    }

}
