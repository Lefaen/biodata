<?php

use yii\db\Migration;
use \common\models\Bonuses;

/**
 * Class m210510_101820_default_data_to_bonuses
 */
class m210510_101820_default_data_to_bonuses extends Migration
{
    public function up()
    {
        $this->batchInsert(Bonuses::tableName(), ['title', 'quantity'], [
            ['Бесплатное обследование', 3],
            ['Скидка на поездку в санаторий', 0],
            ['Кружку с логотипом “БиоДата”', -1],
        ]);
    }

    public function down()
    {
        echo "m210510_101820_default_data_to_bonuses cannot be reverted.\n";

        return false;
    }

}
