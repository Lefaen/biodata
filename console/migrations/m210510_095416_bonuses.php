<?php

use yii\db\Migration;
use \common\models\Bonuses;

/**
 * Class m210510_095416_bonuses
 */
class m210510_095416_bonuses extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Bonuses::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'quantity' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(Bonuses::tableName());

        return false;
    }

}
