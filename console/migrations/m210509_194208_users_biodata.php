<?php

use yii\db\Migration;
use common\models\BiodataUsers;

/**
 * Class m210509_194208_users_biodata
 */
class m210509_194208_users_biodata extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(BiodataUsers::tableName(), [
            'id' => $this->primaryKey(),
            'facebook_id' => $this->string()->notNull()->unique(),
            'bonus_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(BiodataUsers::tableName());
        //return false;
    }

}
