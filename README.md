# README #

За основу брался образ yiisoft/yii2-app-advanced

https://github.com/yiisoft/yii2-app-advanced

В корне проекта для развертывания находится docker-compose

Запускаемые сервисы


frontend    - http://localhost:80

backend     - http://localhost:21080

api         - http://localhost:21081

mysql       - yii2advanced:secret@localhost:3306

adminer     - http://localhost:8080 - host "mysql"