<?php

/* @var $this yii\web\View
 * @var $profile \frontend\models\interfaces\iSocialProfile
 * @var $bonus \common\models\Bonuses
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Имя: <?= $profile->getName(); ?></p>

    <span>Ваш бонус:</span>
    <span>
        <?php if (!$bonus): ?>
            <?php echo
                Html::beginForm(['/site/bonus'], 'post')
                . Html::submitButton(
                    'Получить бонус',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm();
            ?>
        <?php else: ?>
            <?=$bonus->title; ?>
        <?php endif; ?>
        </span>
</div>