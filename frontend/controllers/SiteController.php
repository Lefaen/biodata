<?php

namespace frontend\controllers;

use common\models\BiodataUsers;
use frontend\models\Socials\Facebook;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionProfile()
    {
        $user = new BiodataUsers();
        $profile = $user->getProfile(Facebook::NAME);
        $user = BiodataUsers::getUserByFacebookId($profile->getId());
        return $this->render('profile', ['profile' => $profile, 'bonus' => $user->bonus]);
    }

    public function actionBonus()
    {
        if($id = BiodataUsers::getCurrentUser()){
            $user = BiodataUsers::getUserByFacebookId($id);
            if($user->bonus == null){
                $user->setBonus();
                $user->save();
            }
            $this->redirect('/site/profile');
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $user = new BiodataUsers();

        $user->login(Facebook::NAME);
        if(Yii::$app->request->get()){
            $this->redirect('/site/profile');
        }
        return $this->render('login');
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        $user = new BiodataUsers();
        $user->logout();
        return $this->goHome();
    }
}
