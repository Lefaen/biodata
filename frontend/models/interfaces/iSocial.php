<?php

namespace frontend\models\interfaces;

interface iSocial
{
    public function login($data = null):bool;

    public function logout();

    public function register($data = null);

    public function loggedOn();

    public function getProfile():iSocialProfile;
}