<?php


namespace frontend\models\interfaces;


interface iSocialProfile
{
    public function getId();
    public function setId($id);

    public function getName();
    public function setName($name);

    public function getEmail();
    public function setEmail($email);
}