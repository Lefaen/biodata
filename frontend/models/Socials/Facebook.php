<?php


namespace frontend\models\Socials;

use common\models\BiodataUsers;
use frontend\models\interfaces\iSocialProfile;
use Yii;


class Facebook extends Social
{
    public const NAME = 'facebook';
    private static $redirectUrl = 'http://localhost/site/login';

    private $appId;
    private $versionApi;
    private $code;
    private $user;

    public function __construct($code = '')
    {
        $this->appId = (Yii::$app->params['facebookAppId'])??null;
        $this->versionApi = (Yii::$app->params['facebookVersionApi'])??'v10.0';
        $this->code = Yii::$app->request->get('code');
    }

    public function login($data = null) :bool
    {
        if(!$this->code){
            return false;
        }

        $accessToken = $this->requestAccessToken();
        if(isset($accessToken)){
            $userData = $this->requestUser($accessToken);

            if(isset($userData->id)){
                $user = BiodataUsers::findOne(['facebook_id' => $userData->id]);
                if(!$user){
                    $user = $this->register($userData->id);
                }
            }

            if(isset($data['paramLoggedOn'])){
                Yii::$app->session->set($data['paramLoggedOn'], $user->facebook_id);
            }
        }

        return false;
    }

    public function register($data = null): BiodataUsers
    {
        $user = new BiodataUsers();
        $user->facebook_id = $data;
        $user->save();

        return $user;
    }

    private function requestAccessToken()
    {
        if(Yii::$app->session->get('facebook_token')){
            $accessToken = Yii::$app->session->get('facebook_token');
        }else{
            $data['client_id'] = $this->appId;
            $data['redirect_uri'] = self::$redirectUrl;
            $data['client_secret'] = Yii::$app->params['facebookClientSecret'];
            $data['code'] = $this->code;

            $responce = $this->request('/oauth/access_token', $data);
            if(isset($responce->access_token)) {
                $accessToken = $responce->access_token;
                Yii::$app->session->set('facebook_token', $accessToken);
            }

        }

        return $accessToken;
    }

    private function requestUser($token)
    {
        $data['access_token'] = $token;
        $data['fields'] = 'id,first_name,last_name';
        $responce = $this->request('/me', $data);

        return $responce;
    }

    public function getProfile() :iSocialProfile
    {
        $accessToken = $this->requestAccessToken();
        if(isset($accessToken)){
            $this->user = $this->requestUser($accessToken);
            if($this->user){

                $profile = new SocialProfile();

                if(isset($this->user->id)){
                    $profile->setId($this->user->id);
                }
                if(isset($this->user->first_name) && isset($this->user->last_name)){
                    $profile->setName($this->user->first_name . ' ' . $this->user->last_name);
                }

                return $profile;
            }
        }
    }

    private function request($url, $data = [], $urn = 'https://graph.facebook.com/', $method = 'GET', $redirectUri = null)
    {
        $uri = $urn . $this->versionApi . $url;

        if($method == 'GET'){
            $uri .= '?' . http_build_query($data);
        }

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $result = curl_exec($ch);
        curl_close($ch);

        $result = (json_decode($result));

        return $result;
    }


    public static function getAuthLink(string $contentLink = 'facebook', array $options = []) :string
    {
        $link = '<a ';

        $href = 'https://www.facebook.com/' . ((Yii::$app->params['facebookVersionApi'])??'v10.0') . '/dialog/oauth?' .
            'client_id=' . Yii::$app->params['facebookAppId'] .
            '&redirect_uri=' . self::$redirectUrl;
        $options['href'] = $href;

        foreach ($options as $key => $value)
        {
            $link .= $key . '="' . $value . '" ';
        }

        $link .= '>' . $contentLink . '</a>';

        return $link;
    }
}