<?php

namespace frontend\models\Socials;

use common\models\SocialsList;
use frontend\models\interfaces\iSocial;
use frontend\models\interfaces\iSocialProfile;
use Yii;

class Social implements iSocial
{
    private iSocial $social;
    public static $paramLoggedOn = 'auth_user';

    public function __construct($socialName)
    {
        try {
            $socials = SocialsList::list();
            $this->social = new $socials[$socialName]();
        } catch (\Exception $exception){
            throw new \Exception($socialName . ' not found');
        }
    }

    public function login($data = null) :bool
    {
        $data['paramLoggedOn'] = self::$paramLoggedOn;
        return $this->social->login($data);
    }

    public function logout()
    {
        Yii::$app->session->remove(self::$paramLoggedOn);
    }

    public function register($data = null)
    {
        return $this->social->register($data);
    }

    public function loggedOn()
    {
        Yii::$app->session->get(self::$paramLoggedOn);
    }

    public function getProfile() :iSocialProfile
    {
        return $this->social->getProfile();
    }
}